#!/usr/bin/python
# -*- coding: utf-8 -*-
import utils
import sys
import time
import Ice
import random
from RobotControllerAttacker import RobotControllerAttacker
from RobotControllerDefender import RobotControllerDefender
Ice.loadSlice('drobots.ice')
Ice.loadSlice('factory.ice')
import drobots
import factories

class PlayerI(drobots.Player, Ice.Application):

    controller = None
    broker = None
    nickName = None
    allControllerPrx = {}
    nControllersMade = 0

    def __init__(self, nickName):
        self.nickName = nickName
        self.broker = self.communicator()

    def makeController(self, robot, current=None):
        print("makeController")
        sys.stdout.flush()
        factoryToUse = 'controllerFactory1@Factory1'
        if (self.nControllersMade == 1):
            factoryToUse = 'controllerFactory2@Factory2'
        elif (self.nControllersMade == 2):
            factoryToUse = 'controllerFactory3@Factory3'
        self.nControllersMade += 1
        controllerFactoryPrx = self.broker.stringToProxy(factoryToUse)
        controllerFactoryPrx = factories.ControllerFactoryPrx.checkedCast(controllerFactoryPrx)
        return controllerFactoryPrx.make(self.nickName, robot)

    def win(self, current=None):
        print("win")
        current.adapter.getCommunicator().shutdown()

    def lose(self, current=None):
        print("lose")
        current.adapter.getCommunicator().shutdown()

    def gameAbort(self, current=None):
        print("game abort")
        current.adapter.getCommunicator().shutdown()


class Client(Ice.Application):
    nickName = None
    server = None
    playerPrx = None
    broker = None
    adapter = None
    servant = None

    def run(self, argv):

        self.broker = self.communicator()
        self.nickName = argv[1]
        objetosDrobots = argv[2].split(" ")

        self.adapter = self.broker.createObjectAdapter("Adapter")
        self.servant = PlayerI(self.nickName)
        self.playerPrx = self.adapter.addWithUUID(self.servant)
        prx_id = self.playerPrx.ice_getIdentity()
        direct_prx = self.adapter.createDirectProxy(prx_id)
        self.playerPrx = drobots.PlayerPrx.checkedCast(direct_prx)

        self.adapter.activate()

        gameInProgress = True
        i = 0
        while(gameInProgress and i < len(objetosDrobots)):
            self.serverPrx = self.broker.propertyToProxy(objetosDrobots[i])
            self.server = drobots.GamePrx.checkedCast(self.serverPrx)
            if not self.server:
                raise RuntimeError("Invalid proxy")

            try:
                print("connecting to object " + objetosDrobots[i])
                self.server.login(self.playerPrx, self.nickName)
                print("connected")
                gameInProgress = False

            except drobots.GameInProgress as e:
                if (i == len(objetosDrobots) - 1):
                    i = 0
                    time.sleep(2)
                else:
                    i += 1
                print(str(e) + "\n" + str(time.time()))


        self.shutdownOnInterrupt()
        self.broker.waitForShutdown()

        return 0


app = Client()
sys.exit(app.main(sys.argv))
