#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import atan2, degrees, pi, hypot, cos, sin, radians
import random

def pointToStr(P):
    try:
        P = pointToCoord(P)
    except:
        pass
    P = "x: " + str(P[0]) + " y: " + str(P[1])
    return P


def pointToCoord(P):
    return P.x, P.y


def xyDistPtoP(P1, P2):
    try:
        P1 = pointToCoord(P1)
    except:
        pass
    try:
        P2 = pointToCoord(P2)
    except:
        pass
    return (P2[0]-P1[0], P2[1]-P1[1])


def euclDistPtoP(P1, P2):
    # euclidean norm
    try:
        P1 = pointToCoord(P1)
    except:
        pass
    try:
        P2 = pointToCoord(P2)
    except:
        pass
    return hypot(P1[0] - P2[0], P1[1] - P2[1])


def anglePtoP(P1, P2):
    # tan^-1(dy, dx)
    try:
        P1 = pointToCoord(P1)
    except:
        pass
    try:
        P2 = pointToCoord(P2)
    except:
        pass
    distance = xyDistPtoP(P1, P2)
    rads = atan2(distance[1], distance[0])
    rads %= 2*pi
    return degrees(rads)

def bulletOutOfField(location, angle, distanceToTarget, fieldHeight, fieldWidth):
    try:
        location = pointToCoord(location)
    except:
        pass
    targetX = location[0] + (distanceToTarget * cos(radians(angle)))
    targetY = location[1] + (distanceToTarget * sin(radians(angle)))
    if ((targetX > fieldWidth) or (targetY > fieldHeight)):
        return True
    else:
        return False
def getPointOfLine(angle1, point1):

    x1, y1 = point1[0], point1[1]
    x2 = x1 + (10 * cos(radians(float(angle1))))
    y2 = y1 + (10 * sin(radians(float(angle1))))
    return (int(x2), int(y2))

def intersectionLines(angle1, point1, angle2, point2):
    x1, y1 = point1[0], point1[1]
    x2, y2 = getPointOfLine(angle1, point1)

    x3, y3 = point2[0], point2[1]
    x4, y4 = getPointOfLine(angle2, point2)

    xnum = ((((x1*y2)-(y1*x2))*(x3-x4)) - ((x1-x2)*(x3*y4 - y3*x4)))
    xden = (((x1-x2)*(y3-y4)) - ((y1-y2)*(x3-x4)))
    x = xnum/xden

    ynum = ((x1*y2-y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4))
    yden = ((x1-x2)*(y3-y4)) - ((y1-y2)*(x3-x4))
    y = ynum/yden
    return (x, y)

def calcCannonTarget(angle, wide, location, fieldHeight, fieldWidth):
    minDistance = 80
    maxDistance = 700
    if ((wide != 1) and (wide % 2 == 1)):
        wide += 1
        wide = wide/2
        angleInf = angle - wide
        angleSup = angle + wide
        angle = random.randrange(angleInf, angleSup)

    distance = random.randrange(minDistance, maxDistance)
    while(bulletOutOfField(location, angle, distance, fieldHeight, fieldWidth)):
        try:
            distance = random.randrange(minDistance, distance)
        except ValueError:
            minDistance -= 1
    return angle, distance
