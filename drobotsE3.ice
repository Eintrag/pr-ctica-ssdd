// -*- mode:c++ -*-
#include <drobots.ice>

module drobotsE3 {

  interface RobotControllerAttacker extends drobots::RobotController{
    void tellDetectedRobots(drobots::Point location, string interval);
  };

};
