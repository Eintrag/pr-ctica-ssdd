# **********************************************************************
#
# Copyright (c) 2003-2015 ZeroC, Inc. All rights reserved.
#
# This copy of Ice is licensed to you under the terms described in the
# ICE_LICENSE file included in this distribution.
#
# **********************************************************************
#
# Ice version 3.6.1
#
# <auto-generated>
#
# Generated from file `drobotsE3.ice'
#
# Warning: do not edit this file.
#
# </auto-generated>
#

import Ice, IcePy
import drobots_ice

# Included module drobots
_M_drobots = Ice.openModule('drobots')

# Start of module drobotsE3
_M_drobotsE3 = Ice.openModule('drobotsE3')
__name__ = 'drobotsE3'

if 'RobotControllerAttacker' not in _M_drobotsE3.__dict__:
    _M_drobotsE3.RobotControllerAttacker = Ice.createTempClass()
    class RobotControllerAttacker(_M_drobots.RobotController):
        def __init__(self):
            if Ice.getType(self) == _M_drobotsE3.RobotControllerAttacker:
                raise RuntimeError('drobotsE3.RobotControllerAttacker is an abstract class')

        def ice_ids(self, current=None):
            return ('::Ice::Object', '::drobots::RobotController', '::drobotsE3::RobotControllerAttacker')

        def ice_id(self, current=None):
            return '::drobotsE3::RobotControllerAttacker'

        def ice_staticId():
            return '::drobotsE3::RobotControllerAttacker'
        ice_staticId = staticmethod(ice_staticId)

        def tellDetectedRobots(self, location, interval, current=None):
            pass

        def __str__(self):
            return IcePy.stringify(self, _M_drobotsE3._t_RobotControllerAttacker)

        __repr__ = __str__

    _M_drobotsE3.RobotControllerAttackerPrx = Ice.createTempClass()
    class RobotControllerAttackerPrx(_M_drobots.RobotControllerPrx):

        def tellDetectedRobots(self, location, interval, _ctx=None):
            return _M_drobotsE3.RobotControllerAttacker._op_tellDetectedRobots.invoke(self, ((location, interval), _ctx))

        def begin_tellDetectedRobots(self, location, interval, _response=None, _ex=None, _sent=None, _ctx=None):
            return _M_drobotsE3.RobotControllerAttacker._op_tellDetectedRobots.begin(self, ((location, interval), _response, _ex, _sent, _ctx))

        def end_tellDetectedRobots(self, _r):
            return _M_drobotsE3.RobotControllerAttacker._op_tellDetectedRobots.end(self, _r)

        def checkedCast(proxy, facetOrCtx=None, _ctx=None):
            return _M_drobotsE3.RobotControllerAttackerPrx.ice_checkedCast(proxy, '::drobotsE3::RobotControllerAttacker', facetOrCtx, _ctx)
        checkedCast = staticmethod(checkedCast)

        def uncheckedCast(proxy, facet=None):
            return _M_drobotsE3.RobotControllerAttackerPrx.ice_uncheckedCast(proxy, facet)
        uncheckedCast = staticmethod(uncheckedCast)

        def ice_staticId():
            return '::drobotsE3::RobotControllerAttacker'
        ice_staticId = staticmethod(ice_staticId)

    _M_drobotsE3._t_RobotControllerAttackerPrx = IcePy.defineProxy('::drobotsE3::RobotControllerAttacker', RobotControllerAttackerPrx)

    _M_drobotsE3._t_RobotControllerAttacker = IcePy.defineClass('::drobotsE3::RobotControllerAttacker', RobotControllerAttacker, -1, (), True, False, None, (_M_drobots._t_RobotController,), ())
    RobotControllerAttacker._ice_type = _M_drobotsE3._t_RobotControllerAttacker

    RobotControllerAttacker._op_tellDetectedRobots = IcePy.Operation('tellDetectedRobots', Ice.OperationMode.Normal, Ice.OperationMode.Normal, False, None, (), (((), _M_drobots._t_Point, False, 0), ((), IcePy._t_string, False, 0)), (), None, ())

    _M_drobotsE3.RobotControllerAttacker = RobotControllerAttacker
    del RobotControllerAttacker

    _M_drobotsE3.RobotControllerAttackerPrx = RobotControllerAttackerPrx
    del RobotControllerAttackerPrx

# End of module drobotsE3
