#!/usr/bin/python -u
# -*- coding:utf-8; tab-width:4; mode:python -*-
#include <Ice/Ice.h>
#include "PrinterI.h"

using namespace std;
using namespace Ice;

import sys
import Ice
Ice.loadSlice('container.ice')
import Services


class ContainerI(Services.Container):
    def __init__(self):
        self.proxies = dict()

    def link(self, key, proxy, current=None):
        if key in self.proxies:
            raise Services.AlreadyExists(key)

        print("link: {0} -> {1}".format(key, proxy))
        self.proxies[key] = proxy

    def unlink(self, key, current=None):
        if not key in self.proxies:
            raise Services.NoSuchKey(key)

        print("unlink: {0}".format(key))
        del self.proxies[key]

    def list(self, current=None):
        return self.proxies

        broker = self.communicator()
        servant = ContainerI()

        adapter = broker.createObjectAdapter("ContainerAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity("container1"))

        print(proxy)

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0

class Server: public Application {
  int run(int argc, char* argv[]) {
    Services::ContainerPtr servant = new Example::PrinterI();

    ObjectAdapterPtr adapter =
         communicator()->createObjectAdapter("ContainerAdapter");
    ObjectPrx proxy = adapter->add(
	 servant, communicator()->stringToIdentity("container1"));

    cout << communicator()->proxyToString(proxy) << endl;

    adapter->activate();
    shutdownOnInterrupt();
    communicator()->waitForShutdown();

    return 0;
  }
};

int main(int argc, char* argv[]) {
  Server app;
  return app.main(argc, argv);
}
