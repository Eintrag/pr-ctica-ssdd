import random
import utils
import Ice
import sys
from RobotControllerI import RobotControllerI
Ice.loadSlice("drobotsE3.ice --all -I .")
import drobots
import drobotsE3

class RobotControllerAttacker(RobotControllerI, drobotsE3.RobotControllerAttacker):
    nTurn = 0
    detectedLines = {}

    def __init__(self, playerNickName):
        pass

    def setRobot(self, robot):
        self.robot = robot

    def turn(self, current=None):
        print("Soy attacker")
        print("My turn " + str(self.nTurn))
        self.energy = 100

        if (self.nTurn % 10 == 0):
            self.location = self.getLocation()
            print("Toca cambio")
            self.detectedLines = {}
            destinationP = (random.randrange(0, 999), random.randrange(0, 999))
            self.location = self.robot.end_location(self.location)
            self.isMoving = self.moveRobotToPoint(self.location, destinationP)

        angle = random.randrange(0, 360)
        wide = random.randrange(0, 20)
        print(self.detectedLines)
        print("Energy: " + str(self.energy))

        if (len(self.detectedLines) == 2):
            angle1 = self.detectedLines.items()[0][1].split("wide")[0]
            point1 = utils.pointToCoord(self.detectedLines.items()[0][0])
            angle2 = self.detectedLines.items()[1][1].split("wide")[0]
            point2 = utils.pointToCoord(self.detectedLines.items()[1][0])

            try:
                target = utils.intersectionLines(angle1, point1, angle2, point2)
                self.cannonToPoint(target)
            except:
                print "The lines don't intersect"


        self.nTurn += 1

    def cannonToPoint(self, target):
        while (self.energy >= 50):
            self.energy -= 50
            print("Energy after cannon:" + str(self.energy))
            angle = utils.anglePtoP(self.location, target)
            distance = utils.euclDistPtoP(self.location, target)
            print("Cannon: Angle: %s, Distance: %s" % (angle, distance))
            self.robot.cannon(angle, distance)
        else:
            print ("Need more energy for Cannon")

    def cannon(self, location, fieldHeight, fieldWidth, interval):
        while (self.energy >= 50):
            self.energy -= 50
            print("Energy after cannon:" + str(self.energy))
            print(interval)
            angle, distance = utils.calcCannonTarget(int(interval[0]),
                                                     int(interval[1]),
                                                     location, fieldHeight,
                                                     fieldWidth)
            print("Cannon: Angle: %s, Distance: %s" % (angle, distance))
            self.robot.cannon(angle, distance)
        else:
            print ("Need more energy for Cannon")

    def tellDetectedRobots(self, defenderLocation, interval, current=None):
        self.detectedLines[defenderLocation] = interval
