#!/usr/bin/python
# -*- coding: utf-8 -*-
import gtk
import utils
import sys
import time
import Ice
Ice.loadSlice('drobots.ice')
import drobots


class RobotControllerI(drobots.RobotController):

    robot = None

    def __init__(self, robot):
        self.robot = robot

    def turn(self, current=None):
        print("My turn")
        print utils.pointToStr(self.robot.location())
        #gtk.main()

    def robotDestroyed(self, current=None):
        pass

    def moveRobotToPoint(self, P):
        robotP = utils.pointToCoord(self.robot.location())
        print(utils.pointToStr(robotP))
        distance = utils.euclDistPtoP(robotP, P)
        angle = utils.anglePtoP(robotP, P)
        if (distance > 20):
            speed = 100
        elif (distance > 10):
            speed = 40
        else:
            speed = 10
        print("To Point " + utils.pointToStr(P) + " with speed: " +
              str(speed) + "%")
        self.robot.drive(angle, speed)


class PlayerI(drobots.Player):

    controller = None

    def makeController(self, robot, current=None):
        print "makeController"
        print "robot:"
        print robot
        sys.stdout.flush()

        self.controller = RobotControllerI(robot)
        rControllerPrx = current.adapter.addWithUUID(self.controller)
        rControllerPrx = drobots.RobotControllerPrx.checkedCast(rControllerPrx)

        return rControllerPrx

    def win(self, current=None):
        print "win"
        current.adapter.getCommunicator().shutdown()

    def lose(self, current=None):
        print "lose"
        current.adapter.getCommunicator().shutdown()


class Client(Ice.Application):
    nickName = None
    server = None
    playerPrx = None
    broker = None
    adapter = None
    servant = None

    def run(self, argv):

        self.broker = self.communicator()
        self.nickName = argv[1]
        objetosDrobots = argv[2].split(" ")

        self.adapter = self.broker.createObjectAdapter("Adapter")
        self.servant = PlayerI()
        self.playerPrx = self.adapter.addWithUUID(self.servant)

        self.playerPrx = drobots.PlayerPrx.checkedCast(self.playerPrx)
        self.adapter.activate()

        gameInProgress = True
        i = 0
        while(gameInProgress and i < len(objetosDrobots)):
            self.serverPrx = self.broker.stringToProxy(objetosDrobots[i])
            self.server = drobots.GamePrx.checkedCast(self.serverPrx)
            if not self.server:
                raise RuntimeError("Invalid proxy")

            try:
                print("connecting to object " + objetosDrobots[i])
                self.server.login(self.playerPrx, self.nickName)
                print("connected")
                gameInProgress = False

            except drobots.GameInProgress as e:
                if (i == len(objetosDrobots) - 1):
                    i = 0
                    time.sleep(2)
                else:
                    i += 1
                print(str(e) + "\n" + str(time.time()))


        self.shutdownOnInterrupt()
        self.broker.waitForShutdown()

        return 0
class GUI:

    entryAngle, entrySpeed = None, None
    entryWide, entryDistance = None, None
    entryPointX, entryPointY = None, None

    def __init__(self, app):
        self.app = app
        self.entryAngle = gtk.Entry()
        self.entrySpeed = gtk.Entry()
        self.entryWide = gtk.Entry()
        self.entryDistance = gtk.Entry()
        self.entryPointX = gtk.Entry()
        self.entryPointY = gtk.Entry()
        self.build_gui()

    def build_gui(self):
        vbox = gtk.VBox()
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.resize(200, 100)
        window.add(vbox)
        buttons = {}
        for name in ['attach', 'scan', 'location', 'cannon', 'damage', 'drive',
                     'stop', 'speed', 'go2center', 'end turn', 'go to point']:
            button = gtk.Button(name)
            buttons[name] = button

        hbox1 = gtk.HBox(); vbox.add(hbox1)
        hbox2 = gtk.HBox(); vbox.add(hbox2)
        hbox3 = gtk.HBox(); vbox.add(hbox3)
        hbox4 = gtk.HBox(); vbox.add(hbox4)
        hbox5 = gtk.HBox(); vbox.add(hbox5)
        hbox6 = gtk.HBox(); vbox.add(hbox6)
        hbox7 = gtk.HBox(); vbox.add(hbox7)

        hbox1.add(buttons['location'])
        hbox1.add(buttons['speed'])
        hbox1.add(buttons['damage'])

        hbox2.add(buttons['go2center'])
        hbox2.add(buttons['drive'])
        hbox2.add(buttons['stop'])

        hbox3.add(gtk.Label("Angle:"))
        hbox3.add(self.entryAngle)

        hbox4.add(buttons['scan'])
        hbox4.add(buttons['cannon'])

        hbox5.add(gtk.Label("Speed"))
        hbox5.add(self.entrySpeed)
        hbox5.add(gtk.Label("Distance"))
        hbox5.add(self.entryDistance)
        hbox5.add(gtk.Label("Wide"))
        hbox5.add(self.entryWide)

        hbox6.add(buttons['go to point'])
        hbox6.add(self.entryPointX)
        hbox6.add(self.entryPointY)

        hbox7.add(buttons['end turn'])

        buttons['location'].connect('pressed', self.location_button_clicked)
        buttons['drive'].connect('pressed', self.drive_button_clicked)
        buttons['go2center'].connect('pressed', self.go2center_button_clicked)
        buttons['stop'].connect('pressed', self.stop_button_clicked)
        buttons['end turn'].connect('pressed', gtk.main_quit)
        buttons['go to point'].connect('pressed', self.goToPoint_button_clicked)

        window.show_all()
        window.connect('destroy', gtk.main_quit)

    def go2center_button_clicked(self, wd):
        self.app.servant.controller.moveRobotToPoint((500, 500))

    def stop_button_clicked(self, wd):
        self.app.servant.controller.robot.drive(0, 0)

    def drive_button_clicked(self, wd):
        try:
            angle = int(self.entryAngle.get_text())
            speed = int(self.entrySpeed.get_text())
            print("driving:")
            print("angle: " + str(angle))
            print("speed: " + str(speed))
            sys.stdout.flush()
            self.app.servant.controller.robot.drive(angle, speed)
        except:
            print "error"

    def location_button_clicked(self, wd):
        point = self.app.servant.controller.robot.location()
        print "Location:\n" + utils.pointToStr(point)

    def cannon_button_clicked(self, wd):
        try:
            angle = int(self.entryAngle.get_text())
            distance = int(self.entryDistance.get_text())
            self.app.servant.controller.robot.cannon(angle, distance)
        except:
            print "error"

    def scan_button_clicked(self, wd):
        try:
            angle = int(self.entryAngle.get_text())
            wide = int(self.entryWide.get_text())
            self.app.servant.controller.robot.scan(angle, wide)
        except:
            print "error"

    def speed_button_clicked(self, wd):
        # mejor llamar métodos con controller.robot.speed()
        # o hacer algún "wrapper" desde el controller?
        # qué nombre ponerle?
        # modificar implementacion del location en consecuencia
        print self.app.servant.controller.robot.speed()

    def damage_button_clicked(self, wd):
        print self.app.servant.controller.robot.damage()

    def goToPoint_button_clicked(self, wd):
        P = int(self.entryPointX.get_text()), int(self.entryPointY.get_text())
        print P
        self.app.servant.controller.moveRobotToPoint(P)
        print("Going to point " + utils.pointToStr(P))

app = Client()
gui = GUI(app)

sys.exit(app.main(sys.argv))
