#!/usr/bin/env python
# -*- coding: utf-8 -*-
import utils
import unittest
from hamcrest import assert_that, anything

class TestDistance(unittest.TestCase):

    def testDistance(self):
        distance = utils.euclDistPtoP((397, 417), (500, 500))
        self.assertEqual(int(distance), 132)

class TestIntersection(unittest.TestCase):

    def testGetPointOfLine1(self):
        assert_that(utils.getPointOfLine(180, (2, 2)) == (-8, 2))

    def testGetPointOfLine2(self):
        self.assertEqual(utils.getPointOfLine(90, (2, 2)), (2, 12))

    def testOrtogonal1(self):
        # given:
        angle1, point1 = 180, (2, 2)
        angle2, point2 = 90, (0, 0)
        # when:
        intersection = utils.intersectionLines(angle1, point1, angle2, point2)
        # then:
        self.assertEqual(intersection, (0, 2))
    def testNoCortan(self):
        # given:
        angle1, point1 = 180, (2, 2)
        angle2, point2 = 180, (2, 3)
        # when:
        intersection = utils.intersectionLines(angle1, point1, angle2, point2)
        # then:
        self.assertEqual(intersection, False)

class TestAnglePtoP(unittest.TestCase):

    def testAngle0(self):
        angle = utils.anglePtoP((0, 0), (0, 0))
        self.assertEqual(angle, 0)

    def testAngle90(self):
        angle = utils.anglePtoP((0, 0), (0, 999))
        self.assertEqual(angle, 90)

    def testAngle45(self):
        angle = utils.anglePtoP((0, 0), (1, 1))
        self.assertEqual(angle, 45)

    def testAngle135(self):
        angle = utils.anglePtoP((0, 0), (-1, 1))
        self.assertEqual(angle, 135)

    def testAngle180(self):
        angle = utils.anglePtoP((0, 0), (-999, 0))
        self.assertEqual(angle, 180)

    def testAngle270(self):
        angle = utils.anglePtoP((0, 0), (0, -200))
        self.assertEqual(angle, 270)

class TestCannonUtils(unittest.TestCase):

    def testOutOfField(self):
        # bulletOutOfField(location, angle, distanceToTarget, fieldHeight, fieldWidth):
        isOut = utils.bulletOutOfField((700,700), 90, 100, 800, 800)
        assert(not isOut)

        isOut = utils.bulletOutOfField((980, 980), 0, 100, 999, 999)
        assert(isOut)

        isOut = utils.bulletOutOfField((980, 980), 90, 100, 999, 999)
        assert(isOut)

        isOut = utils.bulletOutOfField((980, 980), 180, 100, 999, 999)
        assert(not isOut)

        isOut = utils.bulletOutOfField((980, 980), 270, 100, 999, 999)
        assert(not isOut)


    def testCalculateCannonRange(self):
        angle, distance = utils.calcCannonTarget(70, 1, (200, 200), 999, 999)
        assert(angle == 70)
        angle, distance = utils.calcCannonTarget(70, 2, (200, 200), 999, 999)
        assert(angle >= 69 and angle <= 71)
        angle, distance = utils.calcCannonTarget(70, 5, (200, 200), 999, 999)
        assert(angle >= 67 and angle <= 73)
        # Esta se saldria
        angle, distance = utils.calcCannonTarget(90, 5, (900, 900), 999, 999)
        assert(distance < 101)
        # Si la distancia posible es mas pequeña que el limite al que se haria
        # daño, que opte por la distancia maxima a la que la bala no se salga
        # del campo
        angle, distance = utils.calcCannonTarget(90, 1, (990, 990), 999, 999)
        assert(distance == 9)


if __name__ == '__main__':
    unittest.main()
