#!/usr/bin/python
# -*- coding: utf-8 -*-
import utils
import Ice
Ice.loadSlice('drobots.ice')
import drobots


class RobotControllerI(drobots.RobotController):
    fieldHeight = 999
    fieldWidth = 999

    detectedRobots = {}
    robot = None
    energy = 0
    nTurn = 0
    location = None
    isMoving = None

    def __init__(self):
        pass

    def setRobot(self, robot):
        self.robot = robot

    def robotDestroyed(self, current=None):
        print(self.robot)
        print("Robot destroyed")

    def turn():
        print "You should implement either an Attacker or a Defender"

    def getLocation(self):
        if (self.energy >= 1):
            self.energy -= 1
            return self.robot.begin_location()
        else:
            print("Energy is not enough to get location")

    def stopRobot(self):
        stopped = False

        if (self.energy >= 1):
            self.energy -= 1
            self.robot.drive(0, 0)
            stopped = True
        else:
            print("Energy is not enough to stop the robot")
        return stopped

    def moveRobotToPoint(self, currentLocation, destinationP):
        moving = False
        if (self.energy >= 60):
            self.energy -= 60
            moving = True
            robotP = utils.pointToCoord(currentLocation)
            print(utils.pointToStr(robotP))
            distance = utils.euclDistPtoP(robotP, destinationP)
            angle = utils.anglePtoP(robotP, destinationP)
            if (distance > 20):
                speed = 100
            elif (distance > 10):
                speed = 40
            else:
                speed = 10
            print("To Point " + utils.pointToStr(destinationP) +
                  " with speed: " + str(speed) + "%")
            self.robot.drive(angle, speed)
        else:
            print("Energy is not enough to drive the robot")
        return moving
