game = "drobots5 drobots6 drobots7"
good = "Client.py"
dumb = "ClientQuieto.py"
nickName = "SergioFR"

runRegistry:
	icegridregistry --Ice.Config=Registry.config &

runFactory1:
	./FactoryServer.py --Ice.Config=FactoryServer1.config "controllerFactory1" &

runFactory2:
	./FactoryServer.py --Ice.Config=FactoryServer2.config "controllerFactory2" &

runFactory3:
	./FactoryServer.py --Ice.Config=FactoryServer3.config "controllerFactory3" &

runContainer:
	./Container.py --Ice.Config=Container.config &

test:
	./testUtils.py

runone:
	echo "Make sure the registry is running"
	./Client.py --Ice.Config=Client.config $(nickName) $(game)

runtwo:
	echo "Make sure the registry is running"
	./Client.py --Ice.Config=Client.config $(nickName) $(game) &
	./Client.py --Ice.Config=Client.config "P2" $(game) >/dev/null &

runfour:
	echo "Make sure the registry is running"
	./Client.py --Ice.Config=Client.config $(nickName) $(game) &
	./Client.py --Ice.Config=Client.config "P2" $(game) >/dev/null &
	./Client.py --Ice.Config=Client.config "P3" $(game) >/dev/null &
	./Client.py --Ice.Config=Client.config "P4" $(game) >/dev/null

clean:
	rm *.pyc

kill:
	./killServer.sh
