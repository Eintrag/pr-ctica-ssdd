import random
import utils
import time
import Ice
import sys
from RobotControllerI import RobotControllerI
Ice.loadSlice('container.ice')
Ice.loadSlice("drobotsE3.ice --all -I .")
import drobotsE3
import Services

class RobotControllerDefender(RobotControllerI, Ice.Application):
    nTurn = 0
    detectedRobots = {}
    attackerProxies = {}
    numAttackers = 2
    container = None
    playerNickName = None

    def __init__(self, playerNickName):
        self.playerNickName = playerNickName
        self.broker = self.communicator()
        containerPrx = self.broker.stringToProxy('container1@ContainerAdapter')
        self.container = Services.ContainerPrx.checkedCast(containerPrx)

    def setRobot(self, robot):
        self.robot = robot

    def turn(self, current=None):
        print("Soy defender")
        self.attackerProxies = {k: v for k, v in self.container.list().items() if k.startswith('Attacker' + self.playerNickName)}
        print self.attackerProxies
        print("My turn " + str(self.nTurn))
        self.energy = 100

        if (self.nTurn % 10 == 0):
            self.location = self.getLocation()
            print("Toca cambio")
            self.detectedRobots = {}
            destinationP = (random.randrange(0, 999), random.randrange(0, 999))
            self.location = self.robot.end_location(self.location)
            self.isMoving = self.moveRobotToPoint(self.location, destinationP)

        if (self.nTurn % 3 == 0):
            self.detectedRobots = {}

        while (not self.detectedRobots and self.energy >= 10):
            self.detectedRobots = self.scan(random.randrange(0, 360), 20)

        print("Energy: " + str(self.energy))

        if (self.detectedRobots):
            for interval in self.detectedRobots:
                print("In: %s, there are: %s robot(s)" % (interval, self.detectedRobots[interval]))
            self.detectedRobots = self.focusScan(self.detectedRobots)
            interval = self.detectedRobots.items()[-1][0]
            print "I know these attackers"
            print self.attackerProxies['Attacker' + self.playerNickName + str(1)]
            print self.attackerProxies['Attacker' + self.playerNickName + str(2)]
            sys.stdout.flush()

            for attackerPrx in self.attackerProxies.values():
                attackerPrx = drobotsE3.RobotControllerAttackerPrx.checkedCast(attackerPrx)
                if(attackerPrx):
                    attackerPrx.tellDetectedRobots(self.location, interval)
                else:
                    print("ERROR FETCHING ATTACKER")

        self.nTurn += 1

    def focusScan(self, detectedRobots):
        interval = detectedRobots.items()[-1][0].split("wide:")
        angle, wide = int(interval[0]), int(interval[1])
        if ((wide/2) % 2 == 1):
            # Si es impar, al dividirlo en dos no abarcariamos todo el intervalo
            wide += 1
        while ((self.energy >= 10) and (wide > 2)):
            print ("Focusing on " + str(interval))
            primeraMitad = (angle - wide/4, wide/2)
            if (not self.scan(primeraMitad[0], primeraMitad[1])):
                # si no hay en esa mitad, sabemos que es en la otra:
                print ("Then it is: %s degrees, %s wide" % (str(angle + wide/4),
                                                           str(wide/2)))
                interval = (str(angle + wide/4) + "wide:" + str(wide/2))
                detectados = 1
                detectedRobots = {}
                print ("Cleaning Detected Robots dictionary except for: " + str(interval))
                detectedRobots[interval] = detectados
            else:
                detectedRobots = {}
                interval = str(angle - wide/4) + "wide:" + str(wide/2)
                detectedRobots[interval] = 1
            interval = detectedRobots.items()[-1][0].split("wide:")
            angle, wide = int(interval[0]), int(interval[1])

        return detectedRobots

    def scan(self, angle, wide):
        detectedRobots = {}
        if (self.energy >= 10):
            detectados = self.robot.begin_scan(angle, wide)
            print ("Scan: %s degrees, %s wide" % (str(angle), str(wide)))
            self.energy -= 10
            interval = (str(angle) + "wide:" + str(wide))
            if (self.robot.end_scan(detectados)):
                print ("Cleaning Detected Robots dictionary except for: " + str(interval))
                detectedRobots[interval] = 1
        else:
            print("Need more energy for Scan")

        return detectedRobots
