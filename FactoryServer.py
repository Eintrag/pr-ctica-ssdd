#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys

import Ice
Ice.loadSlice('drobots.ice')
Ice.loadSlice('factory.ice')
Ice.loadSlice('container.ice')
import drobots
import factories
import Services
from RobotControllerI import RobotControllerI
from RobotControllerAttacker import RobotControllerAttacker
from RobotControllerDefender import RobotControllerDefender

class ControllerFactoryI(factories.ControllerFactory, Ice.Application):
    container = None
    broker = None

    def __init__(self):
        self.broker = self.communicator()
        containerPrx = self.broker.stringToProxy('container1 -t -e 1.1 @ContainerAdapter')
        self.container = Services.ContainerPrx.checkedCast(containerPrx)

    def make(self, nickName, robot, current=None):
        print robot
        if (robot.ice_isA("::drobots::Attacker")):
            print("factory making attacker controller")
            self.controller = RobotControllerAttacker(nickName)
            self.controller.setRobot(robot)
            print("made")

        else:
            print("factory making defender controller")
            self.controller = RobotControllerDefender(nickName)
            self.controller.setRobot(robot)
            print("made")

        sys.stdout.flush()

        rControllerPrx = current.adapter.addWithUUID(self.controller)

        prx_id = rControllerPrx.ice_getIdentity()
        direct_prx = current.adapter.createDirectProxy(prx_id)
        rControllerPrx = drobots.RobotControllerPrx.uncheckedCast(direct_prx)

        if (robot.ice_isA("::drobots::Attacker")):
            isAdded = False
            nAttacker = 1
            while(not isAdded):
                try:
                    self.container.link('Attacker' + nickName +
                                        str(nAttacker), rControllerPrx)
                    isAdded = True
                except Services.AlreadyExists as e:
                    nAttacker += 1

        return rControllerPrx


class Server(Ice.Application):
    container = None

    def run(self, argv):
        broker = self.communicator()
        servant = ControllerFactoryI()

        adapter = broker.createObjectAdapter("ControllerFactoryAdapter")
        proxy = adapter.add(servant,
                            broker.stringToIdentity(argv[1]))

        print(proxy)
        sys.stdout.flush()

        adapter.activate()
        self.shutdownOnInterrupt()

        broker.waitForShutdown()

        return 0


server = Server()
sys.exit(server.main(sys.argv))
